package com.test.sonda.exception;

import java.time.ZonedDateTime;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.test.sonda.model.CustomExceptionModel;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomExceptionHandler {

	@ExceptionHandler(value = { CustomException.class })
	public ResponseEntity<Object> handleCustomException(CustomException e) {

		CustomExceptionModel customEsception = new CustomExceptionModel(e.getMessage(), e.getStatus(),
				ZonedDateTime.now());

		return new ResponseEntity<>(customEsception, e.getStatus());
	}
	@ExceptionHandler(value = { HttpMessageNotReadableException.class })
	public ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e) {

		CustomExceptionModel customEsception = new CustomExceptionModel("El valor del registro no tiene un formato numerico válido", HttpStatus.BAD_REQUEST, ZonedDateTime.now());

		return new ResponseEntity<>(customEsception, HttpStatus.BAD_REQUEST);
	}
}
