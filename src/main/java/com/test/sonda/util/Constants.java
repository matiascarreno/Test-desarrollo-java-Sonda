package com.test.sonda.util;

public final class Constants {

	public static final int MAXIMO_BYTE_TARJETA = 7;
	public static final int MAXIMO_BYTE_IDENTIFICADOR = 2;
	public static final int BUFFER_SIZE = 9;
	public static final String BINARY_PATH = "classpath:OperacionesBinarias.bin";

}
