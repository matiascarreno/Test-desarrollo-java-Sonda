package com.test.sonda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.sonda.model.RegistroModel;
import com.test.sonda.service.RegistroService;

@RestController
public class RegistroController {

	@Autowired
	private RegistroService service;

	@PostMapping("/registro")
	public RegistroModel getRegistro(@Validated @RequestBody RegistroModel registro) {

		return service.getRegistro(registro);
	}

	@PostMapping("/registros")
	public List<RegistroModel> addVehiculos(@Validated @RequestBody List<RegistroModel> registros) {
		return service.getRegistros(registros);
	}

}
