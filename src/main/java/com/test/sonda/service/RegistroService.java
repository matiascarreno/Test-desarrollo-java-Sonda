package com.test.sonda.service;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.test.sonda.exception.CustomException;
import com.test.sonda.model.RegistroModel;
import com.test.sonda.util.Constants;
import com.test.sonda.util.Util;

@Service
public class RegistroService {

	public RegistroModel getRegistro(RegistroModel registro) {

		File file;
		FileInputStream fis;
		try {
			file = ResourceUtils.getFile(Constants.BINARY_PATH);
			fis = new FileInputStream(file);

			byte[] byteRead;

			while ((byteRead = fis.readNBytes(Constants.BUFFER_SIZE)).length > 0) {

				byte[] tarjeta = Arrays.copyOfRange(byteRead, 0, Constants.MAXIMO_BYTE_TARJETA - 1);
				byte[] identificador = Arrays.copyOfRange(byteRead, Constants.MAXIMO_BYTE_TARJETA,
						Constants.BUFFER_SIZE);

				if (Util.convertByteToInt(tarjeta) == registro.getRegistro()) {
					registro.setOperacion(Util.convertByteToInt(identificador));
					break;
				}
			}

			fis.close();

		} catch (Exception e) {
			throw new CustomException("Hubo un error al leer el archivo binario", HttpStatus.FAILED_DEPENDENCY);
		}

		return registro;
	}

	public List<RegistroModel> getRegistros(List<RegistroModel> registros) {

		File file;
		FileInputStream fis;
		List<RegistroModel> registrosResponse = new ArrayList<RegistroModel>();
		try {
			file = ResourceUtils.getFile(Constants.BINARY_PATH);
			fis = new FileInputStream(file);

			byte[] byteRead;

			while ((byteRead = fis.readNBytes(Constants.BUFFER_SIZE)).length > 0) {

				byte[] tarjeta = Arrays.copyOfRange(byteRead, 0, Constants.MAXIMO_BYTE_TARJETA - 1);
				byte[] identificador = Arrays.copyOfRange(byteRead, Constants.MAXIMO_BYTE_TARJETA,
						Constants.BUFFER_SIZE);

				for (RegistroModel registro : registros) {
					System.out.println("registros: " + registros.size());
					if (Util.convertByteToInt(tarjeta) == registro.getRegistro()) {
						registro.setOperacion(Util.convertByteToInt(identificador));
						registrosResponse.add(registro);
						registros.remove(registro);
						System.out.println("registros: " + registros.size());
						break;
					}
				}

			}
			for (RegistroModel registro : registros) {
				registrosResponse.add(registro);
			}

			fis.close();

		} catch (Exception e) {
			throw new CustomException("Hubo un error al leer el archivo binario", HttpStatus.FAILED_DEPENDENCY);
		}

		return registrosResponse;
	}

}
