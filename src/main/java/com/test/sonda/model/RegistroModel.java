package com.test.sonda.model;

import org.springframework.lang.NonNull;

public class RegistroModel {

	@NonNull
	private long registro;
	private long operacion;

	public RegistroModel(long registro) {
		this.registro = registro;
	}

	public RegistroModel() {
	}

	public long getRegistro() {
		return registro;
	}

	public void setRegistro(long registro) {
		this.registro = registro;
	}

	public long getOperacion() {
		return operacion;
	}

	public void setOperacion(long operacion) {
		this.operacion = operacion;
	}

}
