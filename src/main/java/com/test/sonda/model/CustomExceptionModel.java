package com.test.sonda.model;

import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;

public class CustomExceptionModel {

	private final String message;
	private HttpStatus status;
	private final ZonedDateTime timestamp;

	public CustomExceptionModel(String message, HttpStatus status, ZonedDateTime timestamp) {
		super();
		this.message = message;
		this.status = status;
		this.timestamp = timestamp;
	}
	
	public CustomExceptionModel(String message, ZonedDateTime timestamp) {
		super();
		this.message = message;
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public ZonedDateTime getTimestamp() {
		return timestamp;
	}

}
