# Sonda Test Para desarrollo Java

## Compilar el proyecto
	
	./mvnw clean package
	
## ejecutar el proyecto
	
	java -jar target/Sonda-0.0.1-SNAPSHOT.jar

## Estructura del proyecto
    Es la estrctura estándar de Spring Boot.
    Solo es necesario tener un JRE o JDK instalado, preferentemente la versión 8
	
## Los endpint son los siguientes:

### Obtener un numero de operación (POST): http://localhost:8080/registro
		Recibe un objeto JSON con la estructura en el ejemplo, en caso de no encontrar un valor devuelve la operación en 0
		
			Request Body:
			{
				"registro": 200
			}

			Response: 
			{
				"registro": 200,
				"operacion": 1571
			}
### Obtener varios numeros de operación (POST): http://localhost:8080/registros
		Recibe un arreglo de objetos JSON con la estructura en el ejemplo, en caso de no encontrar un valor devuelve la operación en 0
		
			Request Body:
			[
				{
					"registro": 200
				}, {
					"registro": 575
				}, {
					"registro": 215
				}, {
					"registro": 1235
				}, {
					"registro": 1070
				}, {
					"registro": 935
				}
			]

			Response: 
			[
				{
					"registro": 200,
					"operacion": 1571
				}, {
					"registro": 215,
					"operacion": 1669
				}, {
					"registro": 575,
					"operacion": 1304
				}, {
					"registro": 1070,
					"operacion": 294
				}, {
					"registro": 1235,
					"operacion": 2164
				}, {
					"registro": 9345,
					"operacion": 0
				}
			]
### Comentarios
		Me tomé la libretad de hacer el endpoint para consultar multiples registros a la vez :)

		Lo hice post para mapear los datos directamente dentro de el objeto que almacenará las información de respuesta, además así en caso de que se quiera agregar más parametros de entrada se puede siemplemente agregar al bady del JSON, y luego incorporar el resto de parametros al modelo en Java.
## Manejo de errores
	Finalmente usé un manejo de errores custom, en vez de la clase TablasReferenciasException que estaba definida en los métodos que me proporcionaron en el documento de instrucciones.

	Los únicos errores que me pareció necesario validar son los de lectura de el archivo binario, y el de validación de el formato de entrada. Además como se compara contra el modelo, en caso de que alguno de los datos sea invalido se debería gatillar una excepción que se maneja y se muestra como response.

## Comentarios finales
	Finalmente no ocupé los metodos que me facilitaron en el documento Word con las instrucciones, ya que encontré ese bonito método en Stackoverflow que hace lo que necesitaba.

	La mayoría del tiempo cuando programo todos los nombres de las clases, variables, métodos, etc. las pongo en inglés porque esa es la convención que seguimos, en este caso como estaba todo en español, lo hice en español. La verdad ambas me parecen bien... en caso de que ustedes también sigan una convensión similar y haber hecho el test en español les parezca feo :)
		
## GIT Repository
	https://gitlab.com/matiascarreno/Test-desarrollo-java-Sonda